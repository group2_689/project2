// Created by Megan Huebschman, Taimoor
// Updated: 10/02/2022
// main file to test readerCSV class

#include "ReaderCSV.h"

int main()
{
	// create a ReaderCSV object
	ReaderCSV c1("DemoCSV_01.csv", 2, ',');

	//user input on inserting rows and columns
	int insertRowIndex=2;
	int insertColumnIndex=2;

	//printing the size rows and columns of the CSV file
	//c1.printRowSize();
	//c1.printColSize();

	//insert a row
	//c1.InsertRow(insertRowIndex,0);

	//insert a column
	//c1.InsertColumn(insertColumnIndex,0);

	//delete a row
	//c1.delRow(insertRowIndex,-1);

	//delete a column
	//c1.delColumn(insertColumnIndex,-1);
	
	//print an element from the data and its datatype
	//c1.printele(3,3,true);

	// print a particular subset of the data to the console -- Tanmaye
	//c1.printspecial(2,2,1,8,true,10);
	
	// print the header of column #2 -- Tanmaye
	//c1.printheaders(std::to_string(2));
	
	// change the header of column #2 - Tanmaye
	//c1.setheader(1,2,"New Header");
	
	// print all headers -- Tanmaye
	//c1.printheaders("all");

	// print the data in csv to console
	c1.print(true);

	// write a csv file to a file named write.csv
	//c1.writeCSV("write_demo01.csv");


	return 0;
	
}