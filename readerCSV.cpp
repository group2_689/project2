// Created by Megan Huebschman, Suhas, Taimoor and Tanme
// Updated: 10/05/2022
// readerCSV class to handle csv files

#include<fstream>
#include<sstream> // std::stringstream
#include<typeinfo>
#include<cctype>
#include<algorithm>
#include<stdexcept> // std::runtime_error
#include"ReaderCSV.h"
#include <iomanip> // std::left; std::setw
#include<iterator>
#include<iostream>

ReaderCSV::ReaderCSV(std::string fileName, int n, char delim) 
	: name(fileName), numHeader(n), delimiter(delim)
{
	numCol = 0;
	fileread();
}

// method for reading the csv
void ReaderCSV :: fileread()
{
	// input file stream
	std::ifstream csvfile(name);

	// check if file is open
	if (!csvfile.is_open()){
		throw std::runtime_error("Error: could not open file");}
	else
	{
		std::cout << "File opened successfully!"<<std::endl;
	}

	// temp variables for handling csv data
	std::string row, header, val; 
	std::vector<std::string> temp;
	int count = 0;

	if(csvfile.good())
	{	
		if(numHeader != 0)
		{
			// loop over all the header rows
			for (int i = 0; i < numHeader; i++)
			{
				// Read the header row
				std::getline(csvfile, row);

				// string stream to the header row
				std::stringstream s(row);

				// loop throught the string to get header seperated by the delimiter
				while (std::getline(s, header, delimiter)) {
					temp.push_back(header);
					// empty vectors pushed into the data container
					if (i==0){data.emplace_back(std::vector<Cell> {});}
					count++;
				}
				headers.push_back(temp);
				// set numCol
				if (i==0){numCol = count;}
				temp = {};
			}
		}
		else
			// push an empty vector to the header container
			headers = {{}};	
	}

	numRow = 0;

	// read line by line each column
	while(std::getline(csvfile, row))
	{
		// string stream to the current row
		std::stringstream s(row);
		count = 0;

		// push values into columns row wise
		while(std::getline(s, val, delimiter)) 
		{	
			if (numHeader==0&&numRow==0){data.emplace_back(std::vector<Cell> {});}
			getdatatype(val, count);
			//data[numCol].emplace_back(val);
			count++;
		}

		// if there was no header rows set numCol
		if (numCol == 0){numCol = count;}

		// check if the eof was thrown before traversing the col size i.e row with empty cells at last
		if (s.eof()==true && count<numCol)
		{	
			std::string empty = "";
			for (int k = count; k < numCol; k++)
			{
				data[k].emplace_back(empty);
			}
		}
		numRow++;
	}

	// close stream
	csvfile.close();
}	

void ReaderCSV :: getdatatype(std::string s, int idx)
{
	
	//string out;
	if(!s.empty()
			&& (count(s.begin(),s.end(),'e')+count(s.begin(),s.end(),'E'))<=1 
			&& s.front() != 'e' && s.front() != 'E'
			&& s.back() != 'e' && s.back() != 'E'
			&& all_of(s.begin(),s.end(),[](char c) {return (c == 'E' ||c == '-' ||c == 'e' || isdigit(c));})){
		//out="int";
		int out = stoi(s);
		data[idx].emplace_back(out);
	}
	else if(!s.empty() 
			&& count(s.begin(),s.end(),'.')<=1 
			&& (count(s.begin(),s.end(),'e')+count(s.begin(),s.end(),'E'))<=1 
			&& s.front() != 'e' && s.front() != 'E'
			&& s.back() != 'e' && s.back() != 'E'
			&& all_of(s.begin(),s.end(),[](char c) {return (c == 'E' ||c == '-' ||c == 'e' || c == '.' || isdigit(c));})){
		//out = "double";
		double out = stod(s);
		data[idx].emplace_back(out);
	}
	else if(s == "true" || s == "false"){
		bool out;
		if (s == "true"){out = 1;}  //if bool is a series of 1s and 0s, it will be interpreted as an int, but this shouldn't affect the possible bool manipulations
		else {out = 0;}
		data[idx].emplace_back(out);
	}
	else if(s.length() == 1){
		char out = s[0]; 
		data[idx].emplace_back(out);
	}
	else{data[idx].emplace_back(s);}
	//return out;
	return;
}

// write data to a csv file
void ReaderCSV :: writeCSV(std::string writeName)
{
	// output file stream
	std::ofstream csvwritefile(writeName);

	// write header rows
	if (headers[0].size()!=0)
	{
		for (int j = 0; j<headers.size(); j++)
		{
			for (int k = 0; k<headers[j].size(); k++)
			{
				csvwritefile << headers[j][k];
				
				// remove comma at the end of line
				if (k != (headers[j].size()-1)) {csvwritefile << delimiter;};
			}

			// move to the next line 
			csvwritefile << std::endl;
		} 
	}	

	// write csv data to file
	for (int j = 0; j<data[0].size(); j++)
	{
		for (int k = 0; k<data.size(); k++)
		{
			// polymorphic lamda function
			std::visit([&](auto& beta) 
			{
				csvwritefile << beta;

				// remove comma at the end of line
				if (k != (data.size()-1)) {csvwritefile << delimiter;}
			}, data[k][j]); 
		}

		// move to the next line 
		csvwritefile << std::endl;
	}

	// close stream
	csvwritefile.close();
}

//print function: flag indicates whether to print out all of data
void ReaderCSV :: print(bool flag)
{
	std::cout << "File Name: " << name << std::endl;
	std::cout << "Delimeter: " << delimiter << std::endl;
	std::cout << "Number of header rows: " << numHeader << std::endl;
	std::cout << "Number of data rows in the file are: " << numRow << std::endl;
	std::cout << "Number of coloumns in the file are " << numCol << std::endl << std::endl;
	
	//print out data only if flag is true
	if (flag){
		// print header rows
		if (headers[0].size()!= 0)
		{			
			for (int j = 0; j<headers.size(); j++)
			{
				for (int k = 0; k<headers[j].size(); k++)
				{
					std::cout << headers[j][k] <<" ";
				}
				std::cout << "\n";
			}	
		}
		// pretty output
		for (int j = 0; j<data[0].size(); j++)
		{
			for (int k = 0; k<data.size(); k++)
			{
				// polymorphic lamda function
				std::visit([](auto& beta) {std::cout << beta << " ";}, data[k][j]); 
			}
			std::cout << "\n";
		}
	}
	return;
}


//function to get min row size of the data(works for all uneven column length)
int ReaderCSV :: getMinRowSize(){
	int sizeArray[data.size()];
	for(int i=0;i<data.size();i++){
		sizeArray[i]=data[i].size();
	}
	return *std::min_element(sizeArray,sizeArray+data.size());
}

//function to get max row size of the data(works for all uneven column length)
int ReaderCSV :: getMaxRowSize(){
	int sizeArray[data.size()];
	for(int i=0;i<data.size();i++){
		sizeArray[i]=data[i].size();
	}
	return *std::max_element(sizeArray,sizeArray+data.size());

}
//example to insert a row
	void ReaderCSV :: InsertRow(int r,int p){
		
		//input 1D array data with any data type 
		//example array input
		//if row=-1 code will append the input array to end of the column and 
		//row=0 for normal intended insertion at particular row

		//adjusting to c++ indexing from user input
		r=r-1;
		//Array data for insertion, customizable by user (example set)
		int nCol=data.size();
		int a[nCol];
		for(int i=0;i<nCol;i++){a[i]=i;}
        

		//insertion operation
		if(r<getMinRowSize()){
			if(p==-1){
				for(int k=0;k<nCol;k++){
					data[k].resize(data[k].size()+1);
					data[k][data[k].size()-1]=a[k];
				}
			}
			else{
				for(int k=0;k<nCol;k++){
					data[k].resize(data[k].size()+1);
					for(int i=1;i<data[k].size()-r;i++){
						data[k][data[k].size()-i]=data[k][data[k].size()-i-1];
					}
				data[k][r]=a[k];
				}
			}
		}
		else{
			std::cout<<"No row insertion, Index out of bounds"<<std::endl;
		}
			return;
	}
//example to insert a column
	void ReaderCSV :: InsertColumn(int column,int p){

		//input 1D array data with any data type 
		//example array input
		//if col=-1 code will append the input array to end of all rows and 
		//col=0 for normal intended insertion at particular column

		//adjusting c++ indexing from user input
		column=column-1;
		//user defined column size
		int nCol=data[0].size();

		//Array data for insertion, customizable (currently example array)
		int a[nCol];
		for(int i=0;i<nCol;i++){a[i]=i;}
		data.resize(data.size()+1);
		

		//insertion operation
		if(column<data.size()){

			if(p==-1){
				//adding extra column(append)
				data[data.size()-1].resize(nCol);
				for(int i=0;i<data[data.size()-1].size();i++){
					data[data.size()-1][i]=a[i];
				}
			}
			else{
				int l=0;
				for(int k=0;k<data.size()-column;k++){
					data[data.size()-1-l].resize(data[data.size()-2-l].size());
					for(int i=0;i<data[k].size();i++){
						data[data.size()-1-l][i]=data[data.size()-2-l][i];	
					}
					l++;
				}
				data[column].resize(nCol);
				for(int i=0;i<data[column].size();i++){
					data[column][i]=a[i];
				}
			}
		}
		else{
			std::cout<<"No column insertion, Index out of bounds"<<std::endl;
		}
		return;
	}


//function  to delete a row on user specified location
	void ReaderCSV :: delRow(int row,int p){
		
        //converting index to c++ usage
		row=row-1;
		if(row<data[0].size()){

			if(p==-1){
				for(int i=0;i<data.size();i++){
					data[i].erase(data[i].begin()+data[0].size());
				}

			}
			else{
				for(int i=0;i<data.size();i++){
					data[i].erase(data[i].begin()+row);
				}
			}
		}
		else{
			std::cout<<"Index out of bounds"<<std::endl;
		}
		return;
	}


//function  to delete a column on user specified location
	void ReaderCSV :: delColumn(int column,int p){
		//converting index to c++ usage
		column=column-1;

		if(column<data.size()){
			if(p==-1){
				data.erase(data.begin()+data.size());
			}
			else{
				data.erase(data.begin()+column);
			}
		}
		else{
			std::cout<<"Index out of bounds"<<std::endl;
		}
		
		return;
	}	


	

//Method for printing a specified subset block of the entire data -- Tanmaye
void ReaderCSV :: printspecial(int startRow, int endRow, int startCol, int endCol, bool flag, int colWidth)
{
	//Function arguments:
	//{startRow, endRow, startCol, endCol} --> Subset bounds
	//{flag} --> Indicates whether to print the headers
	//{colWidth} --> Width of the printing cell	
	
	//Check if the indices are valid
	int colCount; //No. of columns that the data has 
	int maxRowCount; //Size of the longest column in no. of rows
	
	//Provided index must be within the above bounds
	maxRowCount=getMaxRowSize();
	colCount=data.size();
	
	if(	endRow-startRow>=0 && endCol-startCol>=0 &&
		startRow>0 && endRow<=maxRowCount &&
		startCol>0 && endCol<=colCount) //Check for sane inputs
	{
		std::cout<<"Printing requested block:"<<std::endl;
		std::cout<<std::endl;
		//Check for the include headers flag
		if(flag==true)
		{
			//Print headers when the flag is true
			for (int j = 0; j<numHeader; j++)
			{
				//adding some neatness :)
				std::cout<<std::right<<std::setw(4)<<"H"<<j+1<<" | ";
				for (int k = startCol-1; k<endCol; k++)
				{
					std::cout << std::left<<std::setw(colWidth)<<headers[j][k] <<" | ";
				}
				std::cout << "\n";
			}
		}
		for (int j = startRow-1; j<endRow; j++)
		{
				//adding some neatness :)
				std::cout<<std::right<<std::setw(4)<<j+1<<") | ";
				for (int k = startCol-1; k<endCol; k++)
				{
					// polymorphic lambda function
					// capture the variable colWidth to be accessed inside the lambda expression
					std::visit([colWidth](auto& beta) {std::cout << std::left<<std::setw(colWidth)<<beta << " | ";}, data[k][j]); 
				}
				std::cout << std::endl;
		}
	}
	else
	{
		//print this message when inputs are out of bounds
		std::cout<<"Cannot Print! kindly check the indices.";
	}	
}

//change a header at particular position -- Tanmaye
void ReaderCSV :: setheader(int headerRow, int headerCol,std::string newHeader)
{
/* 
	A little information about the function arguments -
	headerRow 	= >> int input --- row that contains the header you want to change
	headerCol 	= >> int input --- column that contains the header you want to change
	newHeader	= >> int input --- the string that you want to set as the new header at the specified location
*/	if (numHeader==0){
		std::cout<<"The given data does not contain any headers"<<std::endl;
		return;
	}
	headers[headerRow-1][headerCol-1]=newHeader;
	return;
}

//print list of column headers -- Tanmaye
void ReaderCSV :: printheaders(std::string printType)
{
 /* 
	A little information about the function argument -
	printType = "all" >> string input --- if you need to print all the column headers
	printType = "col_num" >> string input --- if you need to print the header of a particular column
*/
	if(numHeader==0)
	{
		std::cout<<"The given data does not contain any headers"<<std::endl;	
	}
	else
	{
		int colCount = data.size(); //no. of columns in the data set (it will be handy to have this at the outset)
		if(printType=="all")
		{
			std::cout<<">> Printing all column headers"<<std::endl;
			std::cout<<"--------------------------------------"<<std::endl;
			for(int i =0;i<numHeader;i++)
			{
				for(int j=0;j<colCount;j++)
					{
						std::cout	<<std::left
										<<std::setw(20)
										<<headers[i][j]<<" | ";
					}
				std::cout<<std::endl;
			}
				std::cout<<std::endl;
		}
		else //user provides a specific column #
		{
			int colNum=std::stoi(printType); //convert string to integer
			if(colNum>0 && colNum<=colCount) //we are making sure the user enters a valid column #
			{
				//loop over all the header rows in the specified column
				std::cout<<" >> Printing all headers for column #"<<colNum<<std::endl;
				for(int i =0;i<numHeader; i++)
				{
					std::cout << headers[i][colNum-1]<<std::endl;
				}
				std::cout<<std::endl;
			}
			else
			{
				//when column # specified by the user is out of bound
				std::cout<<"Please enter a valid column number. Your data only has "<<colCount<<" columns.";
				std::cout<<std::endl;
			}
		}
	}
	return;
}

//print the value and (or) data type of an element specified by its position -- Tanmaye
void ReaderCSV ::printele(int eleRow, int eleCol, bool flag){
	//Function arguments
	//eleRow, eleCol --> row and column of the element
	//flag --> True if you want the datatype of ele to be displayed, false otherwise

	eleCol=eleCol-1;
	eleRow=eleRow-1;
	if(eleCol<data.size() && eleRow<data[0].size()){
	std::cout<<"Printing element at (" <<eleRow<<", "<<eleCol<<")"<<std::endl;
	std::cout<<"Value = ";
	std::visit([](auto& beta) {std::cout << beta;}, data[eleCol][eleRow]);
	std::cout<<std::endl;
	std::cout<<"Datatype = "<<data[eleCol][eleRow].index()<<std::endl;
	if(flag==true){
		//creating a hashmap for storing and accessing data type labels
		std::unordered_map<int, std::string> DataTypeLabels{

			//the order is the same as that which was used while declaring the variant container
			//refer to the readerCSV.h file
				{0, "Double"},
				{1, "String"},
				{2, "Integer"},
				{3, "Boolean"},
				{4, "Character"},
			};
		std::cout<<"Datatype = "<<DataTypeLabels[data[eleCol][eleRow].index()]<<std::endl;
	}
	}
	else{
		std::cout<<"Index out of bounds for function printele() "<<std::endl;
	}
	return;
}

//function to print rowsize
void ReaderCSV ::printRowSize(){
	std::cout<<"Number of Rows = "<<data[0].size()<<std::endl;
}

//function to print rowsize
void ReaderCSV ::printColSize(){
	std::cout<<"Number of Columns = "<<data.size()<<std::endl;
}

