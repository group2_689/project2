<!--- Authors: Sidhdharth Sundar, Taimoor Daud Khan, Megan Huebschman
      Updated: 10/05/22
-->

# **readerCSV**

This repository contains the code for a class for reading and manipulating a csv file. The code was written as a part of a project for the the MEEN-689 Computing Concepts course taught at the Texas A&M University.

## Features

The different features of the readerCSV class includes
* Reading CSV files with different delimiters.
* Reading CSV files with different number of header rows.
* Reading CSV files with different column sizes; empty spaces are read as empty strings
* Parsing the file to determine the correct data type for the data values. The data types that the code can handle are the following:
    * double
    * string
    * int
    * bool
    * char
	* All other data types are parsed as strings
* Storing data into a varaible type container that is able to handle all the aforementioned data types.
* Printing meta information and file contents to the console.
* Writing CSV files.
* Accessing rows and columns using indexes (a range can be specified to access a subset of the data).
* Inserting rows and columns at a specific index.
* Appending rows and columns to the csv file.
* Deleting rows by index specification.

### Limitations

* CSV files with the delimiter as part of the data contained within it can not be read by readerCSV class.
* If there are rows with empty cells at the end of that row, then readerCSV class can only work if a atleast one header row is provided or the first data row should have no empty cell at its tail.

## Compiling and Running
  
The code makes use of the type safe union ```std::variants``` which are part of C++17 and higer, as such, it requires atleast C++17 to function. GCC5 or higer is needed for code compilation, C++17 is the default standard for GCC11 or higher, if using a previous version, the make file needs to explicity provide the ```-std=c++17``` command line flag. 

To compile and run the code make sure that your pwd is correct and simply type the following command in your unix system terminal
```
 make
 ``` 

## Sample Code

The following code snippet shows how to use the readerCSV class to read a file and print the data to the terminal, the sample csv file used for this demonstration is provided below

header1,  header2,   header3
20,       hi,        99
3,        4,         5
a,        b,         c
3,         ,         1

```
#include "ReaderCSV.h"

int main()
{
    // create a ReaderCSV object to read example.csv
    ReaderCSV c1("example.csv");
    
    // print the data in csv to console
    c1.print(true);

    return 0;
}
```

The terminal output for the above code is

![Terminal Output](image.png)


## Tests

[Test files and put their outputs here. I think this section can be skipped.]

## Usage

The readerCSV class has the following attributes

* **name**: string type for the csv filename.
* **delimiter**: char type to specify the delimiter in the csv file, default value is ','.
* **numHeader**: int type for specifying the number of header rows in the csv file, default values is '1'.
* **numCol**: int type for number of columns in the csv file.
* **numRow**: int type for number of rows in the csv file (does not include the header rows).
* **headers**: 2D vector of type string for storing the header rows.
* **Cell**: type alias for variant that is a union of types double, string, int, bool and char.
* **data**: 2D vector of type Cell for storing the csv data.

The different methods implemented in readerCSV class are as follows

### Constructor: readerCSV(filename, n, delim)
The constructor takes in 3 arguments and sets them to name, delimiter and numHeader attributes of the class. Class object is always initialized with the file name, else the compiler will throw a compile time error. The default value for the number of header rows is 1, if needed the user can specify the header rows, a value 0 indicates that there is no header row. Some csv files are semicolon seperated and to deal with such files the user can set delim ot the appropriate delimiter, the default delimiter is a comma (note the program does not work if delimiter is part of the data values in the csv value).

The constructor makes a call to the fileread method to read the csv file that is provided by the user.

### fileread()
The fileread function reads the csv file. It first opens an input file stream and uses a string stream to parse the header rows and store it to 'headers'. It then uses another string stream to parse the data in the csv file. To determine the appropriate data type for the parsed string value the fileread function makes a call to the getdatatype function which then stores the data in the data container. The fileread function also determines and sets the value for numCol and numRow class variables.

### getdatatype(s,idx)
The getdatatype function takes a string and an integer index as input from the fileread function and determines its type as an int, double, bool, char, or string. All other data types are parsed as strings. It then casts the data string to its appropriate type and stores it in the data container using the index.

### writeCSV(filename)
The write function takes in a filename as a string form the user and then writes the data to a csv file (the delimiter used in the output csv file is the same as that provided by the user) usign an output file stream. It uses two nested loopes to write the header rows and the data columns.

### print(flag)
The print function prints the file meta data that includes the file name, assocaited number of header rows, delimiter choice, number of rows and coloumns. It takes in a boolean argument from the user to determine if the user wants csv data to be printed to the terminal. The default value of flag is false, if set to true the csv data is printed to the terminal along with the meta data.

### InsertRow(row,p)
The InsertRow function inserts a row at the user specified location input (type is int) and p=0. if user wanted to insert a row to the last row, then input should be p=-1.As an example an array of numbers has been inserted.Throws exception when input is out of bounds.

### InsertColumn(column,p)
The InsertColumn function inserts a column at the user specified location input (type is int) and p=0. if user wanted to insert a column to the last column, then input should be p=-1.As an example an array of numbers has been inserted.Throws exception when input is out of bounds.

### delRow(row,p)
The delRow function deletes a row at the user specified location input (type is int) and p=0. if user wanted to delete the last row, then input should be p=-1.Throws exception when input is out of bounds.

### delColumn(row,p)
The delColumn function deletes a column at the user specified location input (type is int) and p=0. if user wanted to delete the last column, then input should be p=-1.Throws exception when input is out of bounds.

### printRowSize()
The printRowSize function prints the row size of the CSV file.

### printColSize()
The printRowSize function prints the number of columns of the CSV file.

### printspecial(startRow, endRow, startCol, endCol, flag, colWidth)
This function is useful for printing a subset of the data that is contained in the CSV file. The first four arguments are integers describing the size and location of the subset that you wish to print. The argument 'flag' needs to be true if you wish to see the headers, false otherwise. The argument 'colWidth' is the desired column width for printing.Throws exception when input is out of bounds.Throws exception when input is out of bounds.

### setheader(headerRow, headerCol, newHeader)
This function is useful for changing the value of a element in the headers container specified by its position (headerRow, headerCol). The argument 'newHeader' is a string containing the new value of the header.Throws exception when input is out of bounds.

### printheaders(printType)
This function is useful for printing the data headers. The argument 'printType' needs to be set to "all" if you wish to see all the headers. If you want to print the header of only one particular column then pass std::to_string(n) as the argument where n is an integer representing the column number you wish to query.Throws exception when input is out of bounds.

### printele(eleRow, eleCol, flag)
This function is useful for printing the value and (or) datatype of an element specified by its position (eleRow, eleCol). The argument 'flag' needs to be set to true if you wish to print the datatype of the element along with its value. Throws exception when input is out of bounds.


