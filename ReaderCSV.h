// Created by Megan Huebschman, Suhas, Taimoor and Tanme
// Updated: 10/05/2022
// Header file for ReaderCSV class

// header guards
#ifndef READERCSV_H
#define READERCSV_H

#include<string>
#include<variant> // std::varaint - type safe union
#include<iostream>
#include<vector>

class ReaderCSV
{   
    // class attributes
    private : 
		std::string name; // name of csv file
		char delimiter;
		int numHeader; // number of header rows in flie
		int numCol; // number of columns in file
		int numRow; // number of rows in file
		std::vector<std::vector<std::string>> headers; //vector of vectors containing each row of headers
		 
		// type aliasing for the vaiant cell
		using Cell = std::variant<double, std::string, int, bool, char>; 

		// vector of vectors of varaint daya types where each position contains the data for a specified column
		std::vector<std::vector<Cell>> data;

		// class methods
		// read file & store data
		void fileread();
        
		// return data type
		void getdatatype(std::string, int);

    public :
		// with default value (constructor)
		ReaderCSV(std::string fileName, int n=1, char delim = ',');

		// wrtie csv file
		void writeCSV(std::string);
       
		// manipulate data
		// get(<position>) //get data at particular position
		// getcols() //return number of cols
		// set(<position>,<new data>) //change data at particular position
		// getheaders() //return list of column headers
		// setheader(<position>,<new header>) //change a header at particular position
		
		// print info about data set
		void print(bool=false); 

		//insert a row over a user specified row number
		void InsertRow(int,int);
	
		//insert a column over a user specified column number
		void InsertColumn(int,int);

		//both are same written for internal use (developer's interest)
		//get min row size
		int getMinRowSize();
		//get max row size
		int getMaxRowSize();
		
		//del a row based on user input of row inumber
		void delRow(int,int);

		//del a column based on user input of column number
		void delColumn(int,int);
		
		//Print a subset of the data -- Tanmaye
		void printspecial(int startRow, int endRow, int startCol, int endCol, bool flag, int colWidth);
	
		//Print list of column headers -- Tanmaye
		void printheaders(std::string printType); 
		
		//Change a header at particular position -- Tanmaye
		void setheader(int headerRow, int headerCol,std::string newHeader); 

		//Print the value and (or) data type of an element specified by its position
		void printele(int eleRow, int eleCol, bool flag);

		//getting row size of the data
		void printRowSize();

		//getting Column size of the data
		void printColSize();
};

#endif